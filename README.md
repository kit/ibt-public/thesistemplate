# IBT LaTeX Template for student thesis and disserations

* thesis.tex is the main document
* Start by having a look at thesis.tex and configuring the basic settings in settings.tex
* The template should compile right away
* .gitignore includes thesis.pdf and refbase.bib, change this setting accordingly if you want to include them as well

## Overleaf usage
* Download this repo and use the NewProject/UploadProject function of Overleaf
* **!!!** Mark thesis.tex as the main document (at Menu/MainDocument) **!!!**
* You have to select thesis.txt or any of the subpages in your viewer before compiling

If you identify flaws or have suggestions for improvement, feel free to leave a comment here or commit an improved version in your own branch and talk to the person responsible for the LaTeX template or your supervisor.
