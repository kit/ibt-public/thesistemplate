# Find out if we are on Linux or macOS and determine the correct way to open the pdf
UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
	OPEN = xdg-open
else
	# If not Linux, then we assume macOS: ($(UNAME), Darwin)
	OPEN = open
endif

PROJECT=thesis

.PHONY: $(PROJECT).pdf all clean

all: $(PROJECT).pdf

$(PROJECT).pdf: $(PROJECT).tex
	mkdir -p build/Parts
	latexmk -pdf -pdflatex="pdflatex -interactive=nonstopmode" -use-make -output-directory=build $<

cleanall:
	latexmk -C

clean:
	latexmk -c

view: all
	$(OPEN) build/${PROJECT}.pdf &
