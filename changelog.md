# CHANGELOG
## v1.4 (al128: 2021-05)
* Switched to separate build folder
* Switched to latexmk-based build process
* Adapted IEEEtran citation style to use doi and arXiv, cleaner URL, hide number
* Hide boxes around links

## v1.3 (dn505: 2019-10-16)
* section included accroding to recommendation by AW 

## v1.2 (sp952: 2019-04-10):
* Changed font of table and table captions
* Changed space between figures / tables according to recommendations by KSP			
* Added a fixed space between following float figures 	
* Added a fixed space between figure caption and figure according to recommendations by KSP 
* Changed space between table caption and table according to recommendations by KSP 	
* A5 - Font size of header changed to 8pt (`\footnotesize`), Font size of footer changed to 10pt (`\normalsize`) - This was recommended by KSP.	
* Forced tex to set first line on a given vertical height (Currently, a set command is used. There may be a more optimal way.) 
* Changed style of abbreviations - see abbreviations.tex

## v1.1  Removed page number from Part title page
* Reduced left/right margin to 1.5cm in A5 mode

## v1.0  First public version
